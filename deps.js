// 🌑🐈 Lune ∷ deps.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

export * as L十n from "https://gitlab.com/kibigo/L10n/-/raw/7d5cb516b79e7061c56e5e4142e11ba9b12e596d/mod.js";
export { default as Lemon } from "https://gitlab.com/kibigo/Lemon/-/raw/521e799db7b9f4cb2a18a8e80d035fba994c77f3/mod.js";
export { default as Baron } from "https://gitlab.com/kibicat/baron/-/raw/6d28493895b680ab8e65b02c067da0ef54172a04/mod.js";
