// 🌑🐈 Lune ∷ dev-deps.js
// ====================================================================
//
// Copyright © 2021–2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { xhtmlNamespace } from "./names.js";
import {
  DOMImplementation,
  DOMParser,
  XMLSerializer,
} from "https://esm.sh/@xmldom/xmldom@0.8.2";

{ // Set up “window”.
  const document = new DOMImplementation().createDocument(
    xhtmlNamespace,
    "html",
    null,
  );
  const { documentElement } = document;
  documentElement.appendChild(
    document.createElementNS(xhtmlNamespace, "head"),
  );
  documentElement.appendChild(
    document.createElementNS(xhtmlNamespace, "body"),
  );
  Object.defineProperties(globalThis, {
    document: {
      configurable: false,
      enumerable: true,
      value: document,
      writable: false,
    },
    window: {
      configurable: false,
      enumerable: true,
      value: globalThis,
      writable: false,
    },
  });
}

{ // Polyfill `element.append()`.
  Object.getPrototypeOf(
    document.documentElement,
  ).append = function append(...children) {
    for (const child of children) {
      this.appendChild(
        typeof child === "string"
          ? this.ownerDocument.createTextNode(child)
          : child,
      );
    }
  };
}

{ // Polyfill event listening and dispatch on document.
  const documentListenerMap = new WeakMap();
  Object.assign(
    Object.getPrototypeOf(document),
    {
      addEventListener(name, fn, ...args) {
        if (documentListenerMap.has(this)) {
          documentListenerMap.get(this).addEventListener(
            name,
            fn.bind(this),
            ...args,
          );
        } else {
          const target = new EventTarget();
          documentListenerMap.set(this, target);
          target.addEventListener(name, fn.bind(this), ...args);
        }
      },
      dispatchEvent(...args) {
        if (documentListenerMap.has(this)) {
          documentListenerMap.get(this).dispatchEvent(...args);
        } else {
          const target = new EventTarget();
          documentListenerMap.set(this, target);
          target.dispatchEvent(...args);
        }
      },
      removeEventListener() {
        // Does nothing because the binding above makes this worthless.
      },
    },
  );
}

{ // “Polyfill” custom elements.
  globalThis.HTMLElement = class HTMLElement {};
  globalThis.customElements = { define: () => {} };
}

export { DOMImplementation, DOMParser, XMLSerializer };

export {
  assert,
  assertEquals,
  assertNotEquals,
  assertStrictEquals,
  assertThrows,
  unimplemented,
  unreachable,
} from "https://deno.land/std@0.134.0/testing/asserts.ts";
