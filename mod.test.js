// 🌑🐈 Lune ∷ mod.test.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { Baron } from "./deps.js";
import { assertStrictEquals } from "./dev-deps.js";
import { LuneApplication } from "./elements/mod.js";
import "./mod.js";

Deno.test({
  name: "Custom elements are assigned.",
  fn: () => {
    assertStrictEquals(
      Baron[Baron.customElements]["lune-application"],
      LuneApplication,
    );
  },
});
