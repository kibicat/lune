// 🌑🐈 Lune ∷ mod.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import context from "./context.js";
import { Baron } from "./deps.js";
import * as elements from "./elements/mod.js";

// Set up custom elements.
for (const LuneElement of Object.values(elements)) {
  Baron[Baron.customElements][
    // If `localName` is defined on LuneElement, use that. Otherwise,
    // assume `name` is like "LuneElement" and convert it to
    // `lune-element`.
    "localName" in LuneElement
      ? LuneElement.localName
      : Array.from(function* () {
        const { name } = LuneElement;
        yield name[0].toLowerCase();
        for (const character of name.substring(1)) {
          yield /[A-Z]/u.test(character)
            ? `-${character.toLowerCase()}`
            : character;
        }
      }()).join("")
  ] = LuneElement;
}

{ // Initialize the application on `DOMContentLoaded`. The
  // `DOMContentLoaded` event fires *after* all modules and deferred
  // scripts, so it gives plugins a chance to modify the configuration.

  /** The initialization function. */
  const initialize = () => {
    Baron.initialize(context);
    document.removeEventListener("DOMContentLoaded", initialize);
  };

  document.addEventListener("DOMContentLoaded", initialize);
}
