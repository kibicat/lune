// 🌑🐈 Lune ∷ strings/mod.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { L十n } from "../deps.js";
import en from "./en.json" assert { type: "json" };

const { Proxy: LocalizationProxy } = L十n;

/**
 * An exported object whose `currentLanguage` and `defaultLanguage`
 * properties can be used to get or set the current and default
 * languages.
 *
 * This is a shorthand for
 * `[L十n.Proxy.localizations][L十n.Proxy.currentLanguage]` and
 * `[L十n.Proxy.localizations][L十n.Proxy.defaultLanguage]` on the
 * default export.
 */
export const config = Object.preventExtensions(
  Object.create(null, {
    currentLanguage: {
      configurable: false,
      enumerable: true,
      get: () => localizations[LocalizationProxy.currentLanguage],
      set: ($) => (
        localizations[LocalizationProxy.currentLanguage] = `${$}`
      ),
    },
    defaultLanguage: {
      configurable: false,
      enumerable: true,
      get: () => localizations[LocalizationProxy.defaultLanguage],
      set: ($) => (
        localizations[LocalizationProxy.defaultLanguage] = `${$}`
      ),
    },
  }),
);

/**
 * The localization data, by language.
 *
 * For the time being, this is mutable and exposed (via the
 * `[L十n.Proxy.localizations]` property on the default export). This
 * is probably desirable? (to let userscripts override aspects of the
 * localization).
 */
const localizations = {
  en,
  [LocalizationProxy.currentLanguage]: "en",
  [LocalizationProxy.defaultLanguage]: "en",
};

export default new LocalizationProxy(localizations);
