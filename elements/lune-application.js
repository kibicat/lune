// 🌑🐈 Lune ∷ elements/lune-application.js
// ====================================================================
//
// Copyright © 2022 Margaret KIBI.
//
// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at <https://mozilla.org/MPL/2.0/>.

import { Lemon } from "../deps.js";

export default class LuneApplication extends HTMLElement {
  constructor() {
    super();
    const shadowRoot = this.attachShadow({ mode: "open" });
    shadowRoot.append(
      Lemon.style`
:host{
  Position: Absolute;
  Inset-Block-Start: 0;
  Inset-Block-End: 0;
  Inset-Inline-Start: 0;
  Inset-Inline-End: 0;
}
`,
    );
  }
}
